package com.tonmoy.donee;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tonmoy.donee.model.NeedModel;
import com.tonmoy.donee.utility.AppUtilities;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CreatTaskActivity extends AppCompatActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    Toolbar toolbar;
    LinearLayout priorityHolder, dateHolder, placeHolder, peopleHolder;
    EditText edittextTitle, edittextDescription;
    TextView textViewDate, textViewPriority, textViewPlace;
    FloatingActionButton floatingActionButtonAdd;
    Calendar calendar;
    int year, month, day;
    FirebaseDatabase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_creat_task);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("New TasK");

        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        initializeView();
        database = FirebaseDatabase.getInstance();

        // myRef.setValue("Hello, World!");
        Query myQuery = database.getReference().child("Needs").orderByChild("mDonee").equalTo(AppUtilities.getToken(this));
        if(myQuery==null)  Log.d("tonmoy","null");

        myQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("tonmoy","entered");
                if (dataSnapshot.exists()) {
                    Log.d("tonmoy","exist");

                        Log.d("tonmoy",dataSnapshot.getChildren().toString());

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        Log.d("tonmoy","found1");
                        NeedModel nd=issue.getValue(NeedModel.class);
                        Log.d("tonmoy",nd.title);
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    private void initializeView() {
        textViewDate = (TextView) findViewById(R.id.textViewDate);
        textViewPlace = (TextView) findViewById(R.id.textViewPlace);
        textViewPriority = (TextView) findViewById(R.id.textViewPriority);

        edittextTitle = (EditText) findViewById(R.id.edittextTitle);
        edittextDescription = (EditText) findViewById(R.id.edittextDesc);

        priorityHolder = (LinearLayout) findViewById(R.id.priorityHolder);
        dateHolder = (LinearLayout) findViewById(R.id.dateHolder);
        placeHolder = (LinearLayout) findViewById(R.id.placeHolder);
        peopleHolder = (LinearLayout) findViewById(R.id.assignedPeopleHolder);

        priorityHolder.setOnClickListener(this);
        dateHolder.setOnClickListener(this);
        placeHolder.setOnClickListener(this);
        peopleHolder.setOnClickListener(this);

        floatingActionButtonAdd = (FloatingActionButton) findViewById(R.id.fab);
        floatingActionButtonAdd.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        if (v == dateHolder) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(this, CreatTaskActivity.this, year, month, day);
            datePickerDialog.show();
        } else if (v == floatingActionButtonAdd) {
            DateFormat df = new SimpleDateFormat("MMM dd, yyyy HH:mm:ss");
            Date dateobj = new Date();
            System.out.println(df.format(dateobj));
            String title = edittextTitle.getText().toString();
            String des = edittextDescription.getText().toString();
            String creationDate = df.format(dateobj);
            String expireDate = textViewDate.getText().toString();
            String place = textViewPlace.getText().toString();
            String token = FirebaseAuth.getInstance().getCurrentUser().getUid();;
            NeedModel needModel = new NeedModel(title, des, creationDate, expireDate, place, token, "Active");

            DatabaseReference myRef = database.getReference("Needs");
            DatabaseReference needRef = myRef.push();
            needRef.setValue(needModel);
            Toast.makeText(this,"Task is successfully created",Toast.LENGTH_LONG).show();
           this.finish();
        }else if(v== priorityHolder)
        {
            CharSequence colors[] = new CharSequence[] {"High", "Medium", "Low"};

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Choose Priority");
            builder.setItems(colors, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which) {
                        case 0: textViewPriority.setText("High");
                            break;
                        case 1: textViewPriority.setText("Medium");
                            break;
                        case 2: textViewPriority.setText("Low");
                            break;
                        default:
                            break;
                    }
                }
            });
            builder.show();
        }
        else  if (v == peopleHolder) {
            Toast.makeText(this,"Coming Soon...",Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
        this.year = year;
        this.month = month;
        this.day = dayOfMonth;
        calendar.set(year, month, dayOfMonth);
        DateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy");
        textViewDate.setText(dateFormat.format(calendar.getTime()));

    }
}
