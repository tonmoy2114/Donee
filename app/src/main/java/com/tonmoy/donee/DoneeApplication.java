package com.tonmoy.donee;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

/**
 * Created by tonmoy on 3/14/16.
 */
public class DoneeApplication extends Application {
    static Context mContext;

    @Override
    public void onCreate() {
        Log.d("DB", "Application On Create.");
        super.onCreate();
        DoneeApplication.mContext = getApplicationContext();
        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                mContext).build();
        ImageLoader.getInstance().init(config);
    }

}
