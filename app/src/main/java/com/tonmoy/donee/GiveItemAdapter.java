package com.tonmoy.donee;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tonmoy.donee.interfaces.OnAcceptTaskListener;
import com.tonmoy.donee.interfaces.OnListNecessityFragmentInteractionListener;
import com.tonmoy.donee.model.ReceiveNeedModel;
import com.tonmoy.donee.model.UserModel;

import java.util.List;

public class GiveItemAdapter extends RecyclerView.Adapter<GiveItemAdapter.ViewHolder> {

    public List<ReceiveNeedModel> allNeedList;
    private final OnListNecessityFragmentInteractionListener mListener;
    private OnAcceptTaskListener taskListener;

    public GiveItemAdapter(List<ReceiveNeedModel> items, OnListNecessityFragmentInteractionListener listener, OnAcceptTaskListener taskListener) {
        allNeedList = items;
        mListener = listener;
        this.taskListener=taskListener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_give_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = allNeedList.get(position);
        holder.mTitleTextView.setText(allNeedList.get(position).need.title);
        holder.mDescriptionTextView.setText(allNeedList.get(position).need.description);
        String donarName = "No Donor";
        UserModel user = allNeedList.get(position).user;
        if (user != null && user.name != null) {
            donarName = user.name;
            Log.d("tonmoy", donarName);
            holder.mAcceptButton.setVisibility(View.GONE);
            holder.mPriorityTextView.setText("High");
        }
        else
        {
            holder.mAcceptButton.setVisibility(View.VISIBLE);
            holder.mPriorityTextView.setText("Low");
        }
        holder.mDonarTextView.setText(donarName);
        holder.mDueDateTextView.setText(allNeedList.get(position).need.expireDate);
        holder.mPlaceTextView.setText(allNeedList.get(position).need.place);
//        if (position == 1) {
//            holder.mPriorityImageView.setImageResource(R.drawable.priority_less);
//        } else {
//            holder.mPriorityImageView.setImageResource(R.drawable.ic_emergency_status1);
//        }
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.face)//display stub image until image is loaded
                .build();
        if (user != null) {
            imageLoader.displayImage(user.photoUrl, holder.mDonorImageView, options);

        }

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.mItem);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return allNeedList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public final View mView;
        public final TextView mTitleTextView;
        public final TextView mDescriptionTextView;
        public final ImageView mDonorImageView;
       // public final ImageView mPriorityImageView;
        public final TextView mDonarTextView;
        public final TextView mPriorityTextView;
        public final TextView mDueDateTextView;
        public final TextView mPlaceTextView;
        public final Button mAcceptButton;
        public final Button mCommentButton;
        public final Button mHideButton;
        public ReceiveNeedModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mTitleTextView = (TextView) view.findViewById(R.id.textViewTitle);
            mDescriptionTextView = (TextView) view.findViewById(R.id.textViewDescription);
            mDonorImageView = (ImageView) view.findViewById(R.id.imageViewDonor);
            mDonarTextView = (TextView) view.findViewById(R.id.textviewDonar);
            mDueDateTextView = (TextView) view.findViewById(R.id.textViewDate);
            mPlaceTextView = (TextView) view.findViewById(R.id.textviewPlace);
            mPriorityTextView= (TextView) view.findViewById(R.id.textViewPriority);
          //  mPriorityImageView = (ImageView) view.findViewById(R.id.imageView_emergency_status);
            mAcceptButton = (Button) view.findViewById(R.id.buttonAccept);
            mCommentButton = (Button) view.findViewById(R.id.buttonComment);
            mHideButton = (Button) view.findViewById(R.id.buttonHide);
            mAcceptButton.setOnClickListener(this);
            mCommentButton.setOnClickListener(this);
            mHideButton.setOnClickListener(this);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mTitleTextView.getText() + "'";
        }

        @Override
        public void onClick(View v) {
            if (v.getId() == mAcceptButton.getId()) {
                String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
                ReceiveNeedModel obj=allNeedList.get(getAdapterPosition());
                FirebaseDatabase database = FirebaseDatabase.getInstance();
                database.getReference().child("Needs").child(obj.needId).child("donar").setValue(uid);
                taskListener.onAcceptTask();


            } else  if (v.getId() == mCommentButton.getId()){
                Toast.makeText(v.getContext(), "Coming Soon... ", Toast.LENGTH_SHORT).show();
            }
            else  if (v.getId() == mHideButton.getId()){
                allNeedList.remove(getAdapterPosition());
                notifyDataSetChanged();
                //Toast.makeText(v.getContext(), "Hide PRESSED = " + String.valueOf(getAdapterPosition()), Toast.LENGTH_SHORT).show();
            }
        }
    }

    public void newData(List<ReceiveNeedModel> datas) {
        allNeedList.clear();
        allNeedList.addAll(datas);
        notifyDataSetChanged();
    }
}
