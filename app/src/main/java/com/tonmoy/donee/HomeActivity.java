package com.tonmoy.donee;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.SpannableString;
import android.text.style.TextAppearanceSpan;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.auth.AuthUI;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tonmoy.donee.community.CommunityActivity;
import com.tonmoy.donee.dummy.DummyContent;
import com.tonmoy.donee.interfaces.OnListNecessityFragmentInteractionListener;
import com.tonmoy.donee.model.NeedModel;
import com.tonmoy.donee.model.ReceiveNeedModel;
import com.tonmoy.donee.model.UserInfo;

public class HomeActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, OnListNecessityFragmentInteractionListener {

    ImageView profilePictureView;
    Toolbar toolbar;
    FloatingActionButton fab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("Give");
        setSupportActionBar(toolbar);


        replaceFragment(MyGiveFragment.newInstance(1));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.setItemIconTintList(null);

        //
        Menu menu = navigationView.getMenu();
        MenuItem tools = menu.findItem(R.id.communicate);
        SpannableString s = new SpannableString(tools.getTitle());
        s.setSpan(new TextAppearanceSpan(this, R.style.TextAppearance44), 0, s.length(), 0);
        tools.setTitle(s);
        navigationView.setNavigationItemSelectedListener(this);

        View hView = navigationView.getHeaderView(0);
        profilePictureView = (ImageView) hView.findViewById(R.id.profileImage);
        TextView name = (TextView) hView.findViewById(R.id.textViewDisplayName);
        TextView email = (TextView) hView.findViewById(R.id.textViewEmail);
        FirebaseAuth auth = FirebaseAuth.getInstance();

        UserInfo userInfo = new UserInfo(this);
        name.setText(auth.getCurrentUser().getDisplayName());
        email.setText(auth.getCurrentUser().getEmail());
        //name.setText("Md Monsur Hossain");
       // email.setText("tonmoy@yahoo.com");
        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.mipmap.ic_launcher)//display stub image until image is loaded
                .build();
        if (auth.getCurrentUser().getPhotoUrl() != null) {
            //String imgurl="https://scontent.xx.fbcdn.net/v/t1.0-9/14224747_10210136564673582_7133126295922300939_n.jpg?oh=75199ac1712db886926dc70bae2a27f1&oe=59322318";
            imageLoader.displayImage(auth.getCurrentUser().getPhotoUrl().toString(), profilePictureView, options);
            //imageLoader.displayImage(imgurl, profilePictureView, options);
            //Log.d("TonmoyImage", auth.getCurrentUser().getPhotoUrl().toString());
        }
        navigationView.setCheckedItem(R.id.home);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//                        .setAction("Action", null).show();
                startActivity(new Intent(HomeActivity.this,CreatTaskActivity.class));
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        fab.setVisibility(View.GONE);
        if (id == R.id.nav_home_give) {
            toolbar.setTitle("Give");
            replaceFragment(MyGiveFragment.newInstance(1));
        } else if (id == R.id.nav_receive) {
            toolbar.setTitle("Receive");
            replaceFragment(MyReceiveFragment.newInstance(1));
            fab.setVisibility(View.VISIBLE);
        } else if (id == R.id.nav_people) {
            toolbar.setTitle("People");
            replaceFragment(new PeopleFragment());

        } else if (id == R.id.nav_donee_works) {
            toolbar.setTitle("How it works");
            replaceFragment(new AboutFragment());

        } else if (id == R.id.nav_settings) {
            toolbar.setTitle("Settings");
            replaceFragment(new SettingsFragment());
        } else if (id == R.id.nav_share) {
            toolbar.setTitle("Share");
        } else if (id == R.id.nav_sign_out) {
            AuthUI.getInstance()
                    .signOut(this)
                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                        public void onComplete(@NonNull Task<Void> task) {
                            // user is now signed out
                            startActivity(new Intent(HomeActivity.this, SignInActivity.class));
                            finish();
                        }
                    });

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void replaceFragment(Fragment fragment) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        ft.replace(R.id.home_container, fragment);
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        ft.commit();
    }

    @Override
    public void onListFragmentInteraction(ReceiveNeedModel item) {

    }
}
