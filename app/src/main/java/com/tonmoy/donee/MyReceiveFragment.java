package com.tonmoy.donee;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tonmoy.donee.dummy.DummyContent;
import com.tonmoy.donee.interfaces.OnListNecessityFragmentInteractionListener;
import com.tonmoy.donee.model.NeedModel;
import com.tonmoy.donee.model.ReceiveNeedModel;
import com.tonmoy.donee.model.UserInfo;
import com.tonmoy.donee.model.UserModel;
import com.tonmoy.donee.utility.AppUtilities;
import com.tonmoy.donee.utility.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;


public class MyReceiveFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    private OnListNecessityFragmentInteractionListener mListener;
    FirebaseDatabase database;
    List<ReceiveNeedModel> allNeeds = new ArrayList<ReceiveNeedModel>();
    ReceiveItemAdapter adapter;
    RecyclerView recyclerView;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MyReceiveFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static MyReceiveFragment newInstance(int columnCount) {
        MyReceiveFragment fragment = new MyReceiveFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_list, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(layoutManager);
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            adapter = new ReceiveItemAdapter(allNeeds, mListener,getActivity());
            recyclerView.setAdapter(adapter);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    layoutManager.getOrientation());
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(48));
            //fetch data
            database = FirebaseDatabase.getInstance();
            fetchData();

        }
        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListNecessityFragmentInteractionListener) {
            mListener = (OnListNecessityFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void fetchData() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        // get all need list where donee id is my id
        Query myQuery = database.getReference().child("Needs").orderByChild("mDonee").equalTo(uid);
        if (myQuery == null) Log.d("tonmoy", "null");

        myQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("tonmoy", "entered");
                if (dataSnapshot.exists()) {
                    Log.d("tonmoy", "exist");
                    Log.d("tonmoy", dataSnapshot.getChildren().toString());

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        Log.d("tonmoy", "found1");
                        NeedModel need = issue.getValue(NeedModel.class);
                        String key=issue.getKey();
                        final ReceiveNeedModel receiveNeedModel=new ReceiveNeedModel(need,null,key);
                        database.getReference().child("users").child(need.donar).addValueEventListener(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                UserModel user = dataSnapshot.getValue(UserModel.class);
                                if (user != null) {
                                    receiveNeedModel.user = user;
                                    adapter.notifyDataSetChanged();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
//                        UserModel user=null;
//                        database.getReference().child("users").child(need.donar).setValue(user);
//                        need.donar=user.name;
//                        Log.d("tonmoy",need.title);
                        allNeeds.add(receiveNeedModel);
                    }

                    adapter.notifyDataSetChanged();
                } else
                    Log.d("tonmoy", "no data exist");

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

}
