package com.tonmoy.donee;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.tonmoy.donee.interfaces.OnListNecessityFragmentInteractionListener;
import com.tonmoy.donee.model.NeedModel;
import com.tonmoy.donee.model.ReceiveNeedModel;
import com.tonmoy.donee.model.UserModel;
import com.tonmoy.donee.utility.VerticalSpaceItemDecoration;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class PeopleFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;
    FirebaseDatabase database;
    List<UserModel> allUsers = new ArrayList<UserModel>();
    PeopleItemAdapter adapter;
    RecyclerView recyclerView;

    public PeopleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_people, container, false);
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            recyclerView = (RecyclerView) view;
            LinearLayoutManager layoutManager = new LinearLayoutManager(context);
            if (mColumnCount <= 1) {
                recyclerView.setLayoutManager(layoutManager);
            } else {
                recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
            }

            adapter = new PeopleItemAdapter(allUsers, getActivity());
            recyclerView.setAdapter(adapter);
            DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(recyclerView.getContext(),
                    layoutManager.getOrientation());
            recyclerView.addItemDecoration(dividerItemDecoration);
            recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(48));
            //fetch data
            database = FirebaseDatabase.getInstance();
            fetchData();

        }
        return view;
    }


    public void fetchData() {
        String uid = FirebaseAuth.getInstance().getCurrentUser().getUid();
        Query myQuery = database.getReference().child("users");
        if (myQuery == null) Log.d("tonmoy", "null");

        myQuery.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("tonmoy", "entered");
                if (dataSnapshot.exists()) {
                    Log.d("tonmoy", "exist");
                    Log.d("tonmoy", dataSnapshot.getChildren().toString());

                    for (DataSnapshot issue : dataSnapshot.getChildren()) {
                        // do something with the individual "issues"
                        Log.d("tonmoy", "found1");
                        UserModel user = issue.getValue(UserModel.class);
                        allUsers.add(user);
                    }
                    adapter.notifyDataSetChanged();
                } else
                    Log.d("tonmoy", "no data exist");
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
}
