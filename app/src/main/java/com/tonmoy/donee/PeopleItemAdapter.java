package com.tonmoy.donee;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.tonmoy.donee.interfaces.OnAcceptTaskListener;
import com.tonmoy.donee.interfaces.OnListNecessityFragmentInteractionListener;
import com.tonmoy.donee.model.ReceiveNeedModel;
import com.tonmoy.donee.model.UserModel;

import java.util.List;

public class PeopleItemAdapter extends RecyclerView.Adapter<PeopleItemAdapter.ViewHolder> {

    List<UserModel> allUsers;
    Context mContext;

    public PeopleItemAdapter(List<UserModel> items, Context ct) {
        allUsers = items;
        mContext = ct;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.fragment_people_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = allUsers.get(position);
        holder.mNameTextView.setText(allUsers.get(position).name);
        holder.mEmailTextView.setText(allUsers.get(position).email);

        ImageLoader imageLoader = ImageLoader.getInstance();
        DisplayImageOptions options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.face)//display stub image until image is loaded
                .build();
        imageLoader.displayImage(allUsers.get(position).photoUrl, holder.mUserImageView, options);

    }

    @Override
    public int getItemCount() {
        return allUsers.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mNameTextView;
        public final TextView mEmailTextView;
        public final ImageView mUserImageView;

        public UserModel mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mNameTextView = (TextView) view.findViewById(R.id.textviewName);
            mEmailTextView = (TextView) view.findViewById(R.id.textviewEmail);
            mUserImageView = (ImageView) view.findViewById(R.id.imageviewPhoto);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameTextView.getText() + "'";
        }


    }

    public void newData(List<UserModel> datas) {
        allUsers.clear();
        allUsers.addAll(datas);
        notifyDataSetChanged();
    }
}
