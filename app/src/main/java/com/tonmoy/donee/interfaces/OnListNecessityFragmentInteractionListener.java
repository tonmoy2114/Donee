package com.tonmoy.donee.interfaces;
import com.tonmoy.donee.model.ReceiveNeedModel;

/**
 * Created by tonmoy on 2/13/17.
 */

public interface OnListNecessityFragmentInteractionListener {
    void onListFragmentInteraction(ReceiveNeedModel item);
}
