package com.tonmoy.donee.model;

/**
 * Created by tonmoy on 2/22/17.
 */

public class NeedModel {
    public String title;
    public String description;
    public String createDate;
    public String expireDate;
    public String place;
    public String mDonee;
    public String donar;
    public String status;

    public NeedModel(String title, String description,String createDate, String expireDate, String place, String mDonee, String status) {
        this.title = title;
        this.description = description;
        this.createDate = createDate;
        this.expireDate = expireDate;
        this.place = place;
        this.mDonee = mDonee;
        this.status = status;
        this.donar="";
    }
    public NeedModel()
    {

    }

}
