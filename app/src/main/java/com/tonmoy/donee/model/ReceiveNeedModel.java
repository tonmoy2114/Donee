package com.tonmoy.donee.model;

/**
 * Created by tonmoy on 2/22/17.
 */

public class ReceiveNeedModel {
    public String needId;
    public NeedModel need;
    public UserModel user;
    public ReceiveNeedModel(NeedModel need,UserModel user,String needId) {
        this.user=user;
        this.need=need;
        this.needId=needId;

    }
    public ReceiveNeedModel()
    {

    }

}
