package com.tonmoy.donee.model;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by tonmoy on 4/6/16.
 */
public class UserInfo {
    public String id = "";
    public String name = "";
    public String email = "";
    public String profileImageUrl = "";
    public String loginPlatform = "";
    public int adsd;

    public UserInfo(String id, String name, String email, String profileImageUrl, String loginPlatform) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.profileImageUrl = profileImageUrl;
        this.loginPlatform = loginPlatform;

    }

    public UserInfo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("bd.com.elites.roadsafety", Context.MODE_PRIVATE);
        this.id = prefs.getString("user_id", "");
        this.name = prefs.getString("user_name", "");
        this.email = prefs.getString("user_email", "");
        this.profileImageUrl = prefs.getString("user_image_url", "");
        this.loginPlatform = prefs.getString("user_login_platform", "");

    }

    public void saveUserInfo(Context context) {
        SharedPreferences prefs = context.getSharedPreferences("bd.com.elites.roadsafety", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("user_id", id);
        editor.putString("user_name", name);
        editor.putString("user_email", email);
        editor.putString("user_image_url", profileImageUrl);
        editor.putString("user_login_platform", loginPlatform);
        editor.commit();
    }

}
