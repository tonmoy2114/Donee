package com.tonmoy.donee.model;

/**
 * Created by tonmoy on 2/22/17.
 */

public class UserModel {

    public String name;
    public String email;
    public String photoUrl;
    public String status;

    public UserModel() {

    }

    public UserModel(String name, String email, String photoUrl, String Status) {
        this.name = name;
        this.email = email;
        this.photoUrl = photoUrl;
        this.status = Status;
    }

}
