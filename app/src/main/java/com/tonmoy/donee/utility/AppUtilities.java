package com.tonmoy.donee.utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

//import com.facebook.AccessToken;

import java.io.IOException;
import java.text.DecimalFormat;

/**
 * Created by tonmoy on 1/24/17.
 */
public class AppUtilities {

    public static final String PACKAGE_NAME="com.tonmoy.donee";

    public static Dialog getProgressDialog(final Context ct, String title, String message) {
        ProgressDialog dialog = new ProgressDialog(ct);
        dialog.setTitle(title);
        dialog.setMessage(message);
        dialog.setIndeterminate(true);
        dialog.setCancelable(true);
        dialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((Activity) ct).finish();
                    }
                });
        return dialog;

    }

    public static void showSimpleAlert(final Context context, String title,
                                       String msg, int iconId, final boolean finishActivity) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (iconId != -1)
            builder.setIcon(iconId);
        if (!isStringNullOrEmpty(title))
            builder.setTitle(title);
        builder.setMessage(msg);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                if (finishActivity )
                    ((Activity) context).finish();
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    public static boolean isStringNullOrEmpty(String str) {
        if (str == null || str.isEmpty()) {
            return true;
        }
            return false;
    }



    public static String getRealPathFromURI(Context ct, Uri contentURI) {
        Cursor cursor = ct.getContentResolver().query(contentURI, null, null,
                null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file
            // path
            return contentURI.getPath();
        } else {
            if (cursor.moveToFirst()) {
                int idx = cursor
                        .getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                if (idx == -1)
                    return null;
                String path = cursor.getString(idx);
                cursor.close();
                return path;
            } else {
                return null;
            }
        }
    }

    public static Bitmap decodeSampledBitmapFromResource(String path,
                                                         int reqWidth, int reqHeight) {
        Log.d("imageFragment", "decodeSampledBitmapFromResource path: " + path
                + "width: " + reqWidth + "height" + reqHeight);
        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        // Calculate inSampleSize
        // BitmapFactory.decod
        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        Bitmap result = BitmapFactory.decodeFile(path, options);
        if (result == null)
            return null;
        Matrix mat;
        try {
            mat = rotationMatrix(path);
            Log.d("imageFragment", mat.toString());
        } catch (IOException e1) {
            Log.d("imageFragment", null);
        }
        try {
            result = Bitmap.createBitmap(result, 0, 0, result.getWidth(),
                    result.getHeight(), rotationMatrix(path), true);
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return result;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        Log.d("test", height + "height");
        Log.d("test", width + "width");
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and
            // width
            final int heightRatio = Math.round((float) height
                    / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will
            // guarantee
            // a final image with both dimensions larger than or equal to the
            // requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
            Log.d("test", inSampleSize + "inSampleSize");
        }

        return inSampleSize;
    }

    public static Matrix rotationMatrix(String path) throws IOException {
        // File f = new File(path2.toString());
        ExifInterface exif = new ExifInterface(path);
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_NORMAL);
        Log.d("tonmoy", "orientation: " + orientation);

        int angle = 0;

        if (orientation == 3) {
            angle = 180;
            Log.d("tonmoy", "orientation: " + orientation + " angle: " + angle);
        } else if (orientation == 6) {
            angle = 90;
            Log.d("tonmoy", "orientation: " + orientation + " angle: " + angle);
        } else if (orientation == 8) {
            angle = 270;
            Log.d("tonmoy", "orientation: " + orientation + " angle: " + angle);
        }

        Matrix mat = new Matrix();
        mat.postRotate(angle);
        return mat;
    }


    public static String getToken(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        return prefs.getString("token", null);
    }

    public static void saveToken(Context context, String token) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("token", token);
        editor.commit();
    }

    public static void saveAdminStatus(Context context, boolean status) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isAdmin", status);
        editor.commit();
    }

    public static boolean isAdmin(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean("isAdmin", false);
    }

    public static boolean isAlertOn(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        return prefs.getBoolean("isAlertOn", false);
    }

    public static void saveAlertStatus(Context context, boolean status) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("isAlertOn", status);
        editor.commit();
    }


    public static void clearSharedPref(Context context) {
        SharedPreferences prefs = context.getSharedPreferences(PACKAGE_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.commit();
    }

//    public static boolean isLoggedInWithFacebook() {
//        AccessToken accessToken = AccessToken.getCurrentAccessToken();
//        return accessToken != null;
//    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }
}
